import Combine


public protocol IPlaceRepository: AnyObject {
    func places(term: String) -> AnyPublisher<[Place], Error>
}
