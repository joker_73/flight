import Foundation


public enum DomainFactory {
    public static func airportSearchUseCase(placeRepository: IPlaceRepository) -> IAirportSearchUseCase {
        return AirportSearchUseCase(placeRepository: placeRepository)
    }
}
