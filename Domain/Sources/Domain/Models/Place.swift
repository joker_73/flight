import Foundation


public struct Place {
    public let countryIata: String
    public let location: Coordinate
    public let airportName: String?
    public let iata: String
    public let coordinates: [Double]
    public let cityIata: String?
    public let name: String


    public init(
        countryIata: String,
        location: Coordinate,
        airportName: String?,
        iata: String,
        coordinates: [Double],
        cityIata: String?,
        name: String
    ) {
        self.countryIata = countryIata
        self.location = location
        self.airportName = airportName
        self.iata = iata
        self.coordinates = coordinates
        self.cityIata = cityIata
        self.name = name
    }
}
