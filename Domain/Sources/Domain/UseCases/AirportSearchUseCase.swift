import Combine


public protocol IAirportSearchUseCase: AnyObject {
    func airports(term: String) -> AnyPublisher<[Place], Error>
}


final class AirportSearchUseCase {
    let placeRepository: IPlaceRepository


    init(placeRepository: IPlaceRepository) {
        self.placeRepository = placeRepository
    }
}


extension AirportSearchUseCase: IAirportSearchUseCase {
    func airports(term: String) -> AnyPublisher<[Place], Error> {
        return placeRepository.places(term: term)
    }
}
