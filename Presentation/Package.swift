// swift-tools-version:5.3


import PackageDescription


let package = Package(
    name: "Presentation",

    defaultLocalization: "en",

    platforms: [
        .iOS(.v14),
    ],

    products: [
        .library(name: "Presentation", targets: ["Presentation"]),
    ],

    dependencies: [
        .package(name: "Domain", path: "../Domain"),
        .package(url: "https://github.com/pointfreeco/combine-schedulers", .upToNextMinor(from: "0.1.2")),
    ],

    targets: [
        .target(name: "Presentation", dependencies: [
            "Domain",
            .product(name: "CombineSchedulers", package: "combine-schedulers")
        ]),

        .testTarget(name: "PresentationTests", dependencies: [
            "Presentation",
            "Domain",
        ]),
    ]
)
