import XCTest
import Combine
import Domain


@testable import Presentation


extension AirportSearchUseCaseWrapper.State {
    var name: String {
        switch self {
        case .waiting:
            return "waiting"

        case .loading:
            return "loading"

        case .error(_):
            return "error"
        }
    }
}


final class AirportSearchUseCaseWrapperTests: XCTestCase {
    let scheduler = DispatchQueue.testScheduler

    var airportSearchUseCaseMock: AirportSearchUseCaseMock!
    var airportSearchUseCaseWrapper: AirportSearchUseCaseWrapper!

    var cancellables: Set<AnyCancellable> = []


    override func setUp() {
        super.setUp()

        airportSearchUseCaseMock = AirportSearchUseCaseMock()
        airportSearchUseCaseWrapper = AirportSearchUseCaseWrapper(
            airportSearchUseCase: airportSearchUseCaseMock,
            scheduler: scheduler.eraseToAnyScheduler()
        )
    }


    override func tearDown() {
        super.tearDown()

        cancellables.removeAll()
    }


    func test_basic_functionality() throws {
        let airportsExpectation = expectation(description: "<\(#function):airport>")
        airportsExpectation.expectedFulfillmentCount = 1

        let stateExpectation = expectation(description: "<\(#function):state>")
        stateExpectation.expectedFulfillmentCount = 1

        airportSearchUseCaseWrapper.$airports
            .collect(4)
            .sink { _ in
                XCTFail()
            } receiveValue: {
                XCTAssertEqual($0, [
                    [],
                    [PlaceModels.p0],
                    [PlaceModels.p1, PlaceModels.p2],
                    [],
                ])
                airportsExpectation.fulfill()
            }
            .store(in: &cancellables)

        airportSearchUseCaseWrapper.$state
            .map { $0.name }
            .collect(9)
            .sink { _ in
                XCTFail()
            } receiveValue: {
                XCTAssertEqual($0, [
                    "waiting",

                    "loading",
                    "waiting",

                    "loading",
                    "waiting",

                    "loading",
                    "error",

                    "loading",
                    "waiting",
                ])
                stateExpectation.fulfill()
            }
            .store(in: &cancellables)

        scheduler.advance(by: .seconds(1))
        airportSearchUseCaseMock.send([Places.p0])

        airportSearchUseCaseWrapper.term = "1"
        scheduler.advance(by: .seconds(1))
        airportSearchUseCaseMock.send([Places.p1, Places.p2])

        airportSearchUseCaseWrapper.term = "2"
        scheduler.advance(by: .seconds(1))
        airportSearchUseCaseMock.error()

        airportSearchUseCaseWrapper.term = "3"
        scheduler.advance(by: .seconds(1))
        airportSearchUseCaseMock.send([])

        waitForExpectations(timeout: 5)
    }
}
