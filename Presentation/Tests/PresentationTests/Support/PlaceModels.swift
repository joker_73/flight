import Foundation


@testable import Presentation


enum PlaceModels {
    static let p0 = PlaceModel(place: Places.p0)

    static let p1 = PlaceModel(place: Places.p1)

    static let p2 = PlaceModel(place: Places.p2)
}
