import Foundation


@testable import Presentation


enum CoordinateModels {
    static let c0 = CoordinateModel(coordinate: Coordinates.c0)

    static let c1 = CoordinateModel(coordinate: Coordinates.c1)

    static let c2 = CoordinateModel(coordinate: Coordinates.c2)
}
