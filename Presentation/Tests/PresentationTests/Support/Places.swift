import Domain


@testable import Presentation


enum Places {
    static let p0 = Place(
        countryIata: "00",
        location: Coordinates.c0,
        airportName: "Number 0",
        iata: "000",
        coordinates: [],
        cityIata: nil,
        name: "0"
    )

    static let p1 = Place(
        countryIata: "11",
        location: Coordinates.c1,
        airportName: "Number 1",
        iata: "111",
        coordinates: [],
        cityIata: nil,
        name: "1"
    )

    static let p2 = Place(
        countryIata: "22",
        location: Coordinates.c2,
        airportName: "Number 2",
        iata: "222",
        coordinates: [],
        cityIata: nil,
        name: "2"
    )
}
