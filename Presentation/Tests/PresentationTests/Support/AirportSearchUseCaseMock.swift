import Combine
import Domain
import XCTest


@testable import Presentation


final class AirportSearchUseCaseMock {
    private var result: Result<[Place], Error>?
    private var promise: Future<[Place], Error>.Promise?
    private var queue: DispatchQueue = DispatchQueue(label: "AirportSearchUseCaseMock:Queue")

    func send(_ places: [Place]) {
        handle(.success(places))
    }

    func error(_ error: Error = TestError.error) {
        handle(.failure(error))
    }

    private func handle(_ result: Result<[Place], Error>) {
        queue.sync {
            if let promise = self.promise {
                assert(self.result == nil)
                promise(result)
                self.promise = nil
                return
            }

            if self.result != nil {
                fatalError("Result has been set already. The mock able to handle only one send at a time.")
            }

            self.result = result
        }
    }
}


extension AirportSearchUseCaseMock: IAirportSearchUseCase {
    func airports(term: String) -> AnyPublisher<[Place], Error> {
        return Future { promise in
            self.queue.sync {
                if let result = self.result {
                    assert(self.promise == nil)
                    promise(result)
                    self.result = nil
                    return
                }

                if self.promise != nil {
                    fatalError("Promise has been set already. The mock able to handle only one promise at a time.")
                }

                self.promise = promise
            }
        }
        .eraseToAnyPublisher()
    }
}
