import Domain


@testable import Presentation


enum Coordinates {
    static let c0 = Coordinate(latitude: 0, longitude: 0)

    static let c1 = Coordinate(latitude: 1, longitude: 1)

    static let c2 = Coordinate(latitude: 2, longitude: 2)
}
