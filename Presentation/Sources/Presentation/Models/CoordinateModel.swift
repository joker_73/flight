import Foundation
import CoreLocation
import Domain


struct CoordinateModel: Equatable {
    let latitude: Double
    let longitude: Double
}


extension CoordinateModel {
    var clLocationCoordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}


extension CoordinateModel {
    init(coordinate: Coordinate) {
        self.init(
            latitude: coordinate.latitude,
            longitude: coordinate.longitude
        )
    }
}
