import Foundation
import Domain


struct PlaceModel: Identifiable, Equatable {
    var id: String { "\(countryIata):\(location):\(airportName ?? ""):\(iata):\(cityIata ?? "")" }

    let countryIata: String
    let location: CoordinateModel
    let airportName: String?
    let iata: String
    let coordinates: [Double]
    let cityIata: String?
    let name: String
}


extension PlaceModel: CustomStringConvertible {
    var description: String {
        let airportName = self.airportName.map { $0 + ", " } ?? ""
        return "[\(iata)] \(airportName)\(name)"
    }
}


extension PlaceModel {
    init(place: Place) {
        self.init(
            countryIata: place.countryIata,
            location: CoordinateModel(coordinate: place.location),
            airportName: place.airportName,
            iata: place.iata,
            coordinates: place.coordinates,
            cityIata: place.cityIata,
            name: place.name
        )
    }
}
