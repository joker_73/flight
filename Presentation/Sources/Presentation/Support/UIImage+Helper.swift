import UIKit


extension UIImage {
    convenience init?(nameInModule: String) {
        self.init(named: nameInModule, in: Bundle.module, compatibleWith: nil)
    }
}
