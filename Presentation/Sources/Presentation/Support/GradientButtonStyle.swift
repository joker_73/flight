import SwiftUI


struct GradientButtonStyle: ButtonStyle {
    private let isRound: Bool

    init(round: Bool = true) {
        self.isRound = round
    }


    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .font(Font.title2.weight(.heavy))
            .foregroundColor(.white)
            .padding(.horizontal)
            .frame(
                minWidth: 50,
                idealWidth: 240,
                maxWidth: .infinity,
                minHeight: 50,
                maxHeight: 50,
                alignment: .center
            )
            .background(LinearGradient(
                gradient: Gradient(colors: [.red, .blue, .purple]),
                startPoint: .topLeading,
                endPoint: .bottomTrailing
            ))
            .cornerRadius(isRound ? 25 : 10)
            .shadow(color: Color.purple.opacity(0.8), radius: 4)
            .scaleEffect(configuration.isPressed ? 0.95 : 1.0)
    }
}
