import UIKit


extension UIColor {
    convenience init?(nameInModule: String) {
        self.init(named: nameInModule, in: Bundle.module, compatibleWith: nil)
    }
}
