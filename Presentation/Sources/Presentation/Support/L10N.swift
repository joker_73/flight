import Foundation


public func L10N(_ key: String) -> String {
    return NSLocalizedString(key, bundle: Bundle.module, comment: "")
}
