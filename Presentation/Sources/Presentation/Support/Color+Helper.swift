import SwiftUI


extension Color {
    init(nameInBundle: String) {
        self.init(nameInBundle, bundle: Bundle.module)
    }
}
