import SwiftUI


struct ShakeEffect: GeometryEffect {
    var animatableData: CGFloat


    func effectValue(size: CGSize) -> ProjectionTransform {
        return ProjectionTransform(CGAffineTransform(
            translationX: 10 * sin(2 * .pi * animatableData),
            y: 0
        ))
    }
}
