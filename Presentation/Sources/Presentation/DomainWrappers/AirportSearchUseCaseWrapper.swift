import Combine
import Domain
import CombineSchedulers


final class AirportSearchUseCaseWrapper: ObservableObject {
    enum State {
        case waiting
        case loading
        case error(Error)
    }


    // MARK: - Input
    @Published var term: String = ""

    // MARK: - Output
    @Published private(set) var airports: [PlaceModel] = []
    @Published private(set) var state: State = .waiting

    // MARK: -
    private let airportSearchUseCase: IAirportSearchUseCase?
    private var cancellables: Set<AnyCancellable> = []


    init(
        airportSearchUseCase: IAirportSearchUseCase?,
        scheduler: AnySchedulerOf<DispatchQueue> = DispatchQueue.main.eraseToAnyScheduler()
    ) {
        self.airportSearchUseCase = airportSearchUseCase

        $term
            .debounce(for: .seconds(0.2), scheduler: scheduler)
            .map { $0.trimmingCharacters(in: .whitespacesAndNewlines) }
            .removeDuplicates()
            .flatMap(maxPublishers: .max(1)) { [weak self] term -> AnyPublisher<[PlaceModel], Never> in
                guard let airportSearchUseCase = self?.airportSearchUseCase else {
                    self?.state = .waiting
                    return Empty().eraseToAnyPublisher()
                }

                self?.state = .loading
                return airportSearchUseCase.airports(term: term)
                    .map { $0.map { PlaceModel(place: $0) } }
                    .catch { [weak self] error -> Empty<[PlaceModel], Never> in
                        self?.state = .error(error)
                        return .init()
                    }
                    .eraseToAnyPublisher()
            }
            .sink(receiveValue: { [weak self] in
                self?.state = .waiting
                self?.airports = $0
            })
            .store(in: &cancellables)
    }
}
