import SwiftUI


struct PlaceView: View {
    var place: PlaceModel

    @Binding var matching: String

    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 6) {
                if let airportName = place.airportName {
                    HighlightedText(airportName, matching: matching)
                } else {
                    Text(L10N("PlaceView.Airport name.Placeholder"))
                }

                HighlightedText(place.name, matching: matching)
                    .font(Font.system(.subheadline))
            }

            Spacer()

            HighlightedText(place.iata, matching: matching)
                .font(.system(.body, design: .monospaced))
        }
        .frame(minHeight: 60)
        .contentShape(Rectangle())
    }
}
