// https://www.thetopsites.net/article/59426359.shtml

import SwiftUI


struct HighlightedText: View {
    let text: String
    let matching: String
    let caseInsensitiv: Bool

    init(_ text: String, matching: String, caseInsensitiv: Bool = true) {
        self.text = text
        self.matching = matching
        self.caseInsensitiv = caseInsensitiv
    }

    var body: some View {
        let pattern: String = NSRegularExpression.escapedPattern(for: matching)
            .trimmingCharacters(in: .whitespacesAndNewlines)
            .folding(options: .regularExpression, locale: .current)
        let options: NSRegularExpression.Options = caseInsensitiv ? .caseInsensitive : .init()
        guard  let regex = try? NSRegularExpression(pattern: pattern, options: options) else {
            return Text(text)
        }

        let range = NSRange(location: 0, length: text.count)
        let matches = regex.matches(in: text, options: .withTransparentBounds, range: range)

        return text.enumerated().map { (char) -> Text in
            guard matches.filter( {
                $0.range.contains(char.offset)
            }).count == 0 else {
                return Text( String(char.element) ).foregroundColor(.purple)
            }
            return Text( String(char.element) )

        }.reduce(Text("")) { (a, b) -> Text in
            return a + b
        }
    }
}
