import SwiftUI


struct WatchFlightView: View {
    @Binding var source: PlaceModel
    @Binding var destination: PlaceModel

    @State var isFlightFullyVisible: Bool = true
    @State var isAutoFlightEnabled: Bool = true

    @Environment(\.presentationMode) private var presentationMode: Binding<PresentationMode>

    var body: some View {
        ZStack {
            FlightView(
                source: $source,
                destination: $destination,
                isFlightFullyVisible: $isFlightFullyVisible,
                isAutoFlightEnabled: $isAutoFlightEnabled
            )
            .edgesIgnoringSafeArea(.all)

            VStack(spacing: 10) {
                HStack {
                    Button {
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        Image(systemName: "chevron.left")
                    }

                    Spacer().layoutPriority(2)
                }

                Spacer()

                HStack {
                    VStack(spacing: 10) {
                        Button {
                            isAutoFlightEnabled = true
                        } label: {
                            Image(systemName: "goforward")
                        }

                        Button {
                            isFlightFullyVisible = true
                        } label: {
                            Image(systemName: "target")
                        }
                    }

                    Spacer().layoutPriority(2)
                }
            }
            .padding()
            .buttonStyle(GradientButtonStyle())
        }
        .navigationBarHidden(true)
    }
}
