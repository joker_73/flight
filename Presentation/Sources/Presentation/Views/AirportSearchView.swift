import SwiftUI


struct AirportSearchView: View {
    @Binding var airport: PlaceModel?
    @Binding var title: String

    @EnvironmentObject private var airportSearchUseCase: AirportSearchUseCaseWrapper

    @Environment(\.presentationMode) private var presentationMode: Binding<PresentationMode>

    @State private var isErrorAlertPresented: Bool = false

    var body: some View {
        VStack(spacing: 10) {
            SearchBarView(title: title, text: $airportSearchUseCase.term) {
                presentationMode.wrappedValue.dismiss()
            }
            .padding(.top)
            .padding(.horizontal)

            if airportSearchUseCase.airports.isEmpty {
                Spacer()

                switch airportSearchUseCase.state {
                case .waiting:
                    Text("🤷‍♂️📭").font(.system(size: 64))
                    Text(L10N("SearchPlaceView.Nothing found placeholder"))

                case .loading:
                    ProgressView()
                        .scaleEffect(2, anchor: .center)
                        .progressViewStyle(CircularProgressViewStyle(tint: .purple))

                case .error(let error):
                    Text("😔").font(.system(size: 64))
                    Text(error.localizedDescription)
                }

                Spacer()
            } else {
                List(airportSearchUseCase.airports) { airport in
                    PlaceView(place: airport, matching: $airportSearchUseCase.term)
                        .onTapGesture {
                            self.airport = airport
                            presentationMode.wrappedValue.dismiss()
                        }
                }
                .edgesIgnoringSafeArea(.bottom)
            }
        }
        .animation(.default)
    }
}


struct AirportSearchView_Previews: PreviewProvider {
    static var previews: some View {
        AirportSearchView(airport: .constant(nil), title: .constant("Title"))
            .environmentObject(AirportSearchUseCaseWrapper(airportSearchUseCase: nil))
            .preferredColorScheme(.dark)
    }
}
