import SwiftUI


struct FlightSearchView: View {
    @EnvironmentObject private var airportSearchUseCase: AirportSearchUseCaseWrapper

    @State private var departureAirport: PlaceModel?
    @State private var arrivalAirport: PlaceModel?
    @State private var isNextViewPushed: Bool = false
    @State private var departureAnimatableData: CGFloat = 0
    @State private var arrivalAnimatableData: CGFloat = 0
    @State private var isDeparturePlaceSearchShown: Bool = false
    @State private var isArrivalPlaceSearchShown: Bool = false

    var body: some View {
        NavigationView {
            VStack(spacing: 20) {
                VStack {
                    makeAirportView(
                        airport: $departureAirport,
                        isSearchSheetPresented: $isDeparturePlaceSearchShown,
                        title: L10N("SearchFlightView.Choice place.Departure.Title"),
                        placeholder: L10N("SearchFlightView.Choice place.Departure.Placeholder"),
                        shakeAnimatableData: departureAnimatableData
                    )

                    makeSwapButton()

                    makeAirportView(
                        airport: $arrivalAirport,
                        isSearchSheetPresented: $isArrivalPlaceSearchShown,
                        title: L10N("SearchFlightView.Choice place.Destination.Title"),
                        placeholder: L10N("SearchFlightView.Choice place.Destination.Placeholder"),
                        shakeAnimatableData: arrivalAnimatableData
                    )
                }
                .padding()
                .background(Color(nameInBundle: "CustomBackground"))
                .cornerRadius(10)

                makeGoButton()
            }
            .padding()
            .navigationBarHidden(true)
            .navigationBarTitle(L10N("SearchFlightView.Choice place.Title"))
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }


    private func makeAirportView(
        airport: Binding<PlaceModel?>,
        isSearchSheetPresented: Binding<Bool>,
        title: String,
        placeholder: String,
        shakeAnimatableData: CGFloat
    ) -> some View {
        HStack(spacing: 16) {
            Image(systemName: "mappin.and.ellipse")
                .font(Font.largeTitle.weight(.regular))
                .foregroundColor(.purple)

            if let place = airport.wrappedValue {
                PlaceView(place: place, matching: .constant(""))
            } else {
                HStack {
                    Text(placeholder)
                    Spacer()
                }
            }
        }
        .sheet(isPresented: isSearchSheetPresented) {
            AirportSearchView(airport: airport, title: .constant(title))
                .environmentObject(airportSearchUseCase)
        }
        .onTapGesture { isSearchSheetPresented.wrappedValue.toggle() }
        .frame(minHeight: 60)
        .modifier(ShakeEffect(animatableData: shakeAnimatableData))
    }

    private func makeSwapButton() -> some View {
        HStack {
            VStack {
                Divider()
            }

            if (departureAirport != nil) || (arrivalAirport != nil) {
                Button {
                    withAnimation(.default) {
                        swap(&departureAirport, &arrivalAirport)
                    }
                } label: {
                    Image(systemName: "arrow.up.arrow.down.circle")
                        .font(Font.largeTitle.weight(.regular))
                        .foregroundColor(.purple)
                }
            }
        }
    }

    private func makeGoButton() -> some View {
        HStack {
            if let departurePlace = Binding($departureAirport),
               let arrivalPlace = Binding($arrivalAirport) {

                NavigationLink(
                    destination: WatchFlightView(source: departurePlace, destination: arrivalPlace),
                    isActive: $isNextViewPushed,
                    label: { EmptyView() }
                )
            } else {
                EmptyView()
            }

            Button(L10N("SearchFlightView.Choice place.Go button.Title")) {
                guard (departureAirport == nil)
                        || (arrivalAirport == nil)
                        || (departureAirport == arrivalAirport) else {
                    isNextViewPushed.toggle()
                    return
                }

                withAnimation(.default) {
                    if (departureAirport == nil) || (departureAirport == arrivalAirport) {
                        departureAnimatableData += 1
                    }

                    if (arrivalAirport == nil) || (departureAirport == arrivalAirport) {
                        arrivalAnimatableData += 1
                    }
                }
            }
            .buttonStyle(GradientButtonStyle(round: false))
            .cornerRadius(10)
        }
    }
}


struct AirportSelectionView_Previews: PreviewProvider {
    static var previews: some View {
        FlightSearchView()
            .environmentObject(AirportSearchUseCaseWrapper(airportSearchUseCase: nil))
            .preferredColorScheme(.dark)
    }
}
