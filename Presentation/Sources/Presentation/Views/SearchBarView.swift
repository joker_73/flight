// https://www.appcoda.com/swiftui-search-bar/

import SwiftUI


struct SearchBarView: View {
    var title: String
    @Binding var text: String

    var cancelHandler: () -> Void

    var isCancelButtonAlwaysShown: Bool = true

    @State private var isEditing: Bool = false

    var body: some View {
        HStack {
            TextField(title, text: $text)
                .padding(7)
                .padding(.horizontal, 25)
                .background(Color(.systemGray3))
                .cornerRadius(8)
                .overlay(
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(Color(.systemGray))
                            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                            .padding(.leading, 8)

                        if isEditing {
                            Button {
                                text = ""
                            } label: {
                                Image(systemName: "multiply.circle.fill")
                                    .foregroundColor(.gray)
                                    .padding(.trailing, 8)
                            }
                        }
                    }
                )
                .onTapGesture {
                    isEditing = true
                }

            if isEditing || isCancelButtonAlwaysShown {
                Button {
                    isEditing = false
                    text = ""
                    cancelHandler()
                } label: {
                    Text(L10N("SearchBarView.Cancel button.Title"))
                        .foregroundColor(.purple)
                }
                .transition(.move(edge: .trailing))
                .animation(.default)
            }
        }
    }
}


struct SearchBarView_Previews: PreviewProvider {
    static var previews: some View {
        SearchBarView(title: "title", text: .constant("Search"), cancelHandler: { })
            .preferredColorScheme(.dark)
    }
}
