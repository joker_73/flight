import UIKit
import MapKit


final class PlaneAnnotationView: MKAnnotationView {
    private let imageView: UIImageView = .init(image: UIImage(nameInModule: "Plane"))


    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }


    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.transform = .identity
    }

    override func prepareForDisplay() {
        super.prepareForDisplay()

        updateAngle()
    }


    func updateAngle() {
        guard let annotation = self.annotation as? PlaneAnnotation,
              let x0 = annotation.mapPoint?.x,
              let y0 = annotation.mapPoint?.y,
              let x1 = annotation.nextMapPoint?.x,
              let y1 = annotation.nextMapPoint?.y else { return }

        let angle = atan2(y1 - y0, x1 - x0)

        imageView.transform = CGAffineTransform.identity
            .rotated(by: CGFloat(angle))
    }
}


private extension PlaneAnnotationView {
    func setupView() {
        translatesAutoresizingMaskIntoConstraints = false

        imageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageView)

        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalTo: widthAnchor),
            imageView.heightAnchor.constraint(equalTo: heightAnchor),
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
}
