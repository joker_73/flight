import MapKit


extension MKPolyline {
    static func bezier(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) -> MKPolyline {
        let _source = MKMapPoint(source)
        let _destination = nearestPoint(source: _source, destination: destination)

        let cp1 = controlPoint(_source, _destination)
        let cp2 = controlPoint(_destination, _source)

        // TODO: добавить определение оптимального количества точек на основаннии длинны маршрута
        let porints = stride(from: 0, through: 1, by: 0.001).map {
            cubicBezierPoint(from: _source, to: _destination, cp1: cp1, cp2: cp2, t: $0)
        }

        return MKPolyline(points: porints, count: porints.count)
    }
}


private func controlPoint(_ a: MKMapPoint, _ b: MKMapPoint) -> MKMapPoint {
    let point = line(from: a, to: b, t: 0.6)
    return rotate(point: point, origin: a, angle: -.pi / 3)
}


private func nearestPoint(source: MKMapPoint, destination: CLLocationCoordinate2D) -> MKMapPoint {
    let _destination = MKMapPoint(destination)
    let destinations = [
        _destination,
        MKMapPoint(x: _destination.x + MKMapSize.world.width, y: _destination.y),
        MKMapPoint(x: _destination.x - MKMapSize.world.width, y: _destination.y),
    ]

    let nearestPoint = destinations.min { a, b in
        let middlePointA = line(from: source, to: a, t: 0.5)
        let middlePointB = line(from: source, to: b, t: 0.5)
        return distance(a: source, b: middlePointA) < distance(a: source, b: middlePointB)
    }

    guard let _nearestPoint = nearestPoint else {
        assertionFailure("The nearest point is undefined")
        return _destination
    }

    return _nearestPoint
}


private func distance(a: MKMapPoint, b: MKMapPoint) -> Double {
    return sqrt(pow(b.x - a.x, 2) + pow(b.y - a.y, 2))
}

private func rotate(point: MKMapPoint, origin: MKMapPoint, angle: Double) -> MKMapPoint {
    // Перенос точки в началало координат, поворот и возврат из начала координат
    return MKMapPoint(
        x: origin.x + (point.x - origin.x) * cos(angle) - (point.y - origin.y) * sin(angle),
        y: origin.y + (point.x - origin.x) * sin(angle) + (point.y - origin.y) * cos(angle)
    )
}

private func line(from: MKMapPoint, to: MKMapPoint, t: Double) -> MKMapPoint {
    let k = (to.y - from.y) / (to.x - from.x)
    let b = (to.x * from.y - to.y * from.x) / (to.x - from.x)

    let x = from.x + (to.x - from.x) * t

    return MKMapPoint(x: x, y: k * x + b)
}


private func cubicBezierPoint(from: MKMapPoint, to: MKMapPoint, cp1: MKMapPoint, cp2: MKMapPoint, t: Double) -> MKMapPoint {
    let x = cubicBezier(t, from.x, cp1.x, cp2.x, to.x)
    let y = cubicBezier(t, from.y, cp1.y, cp2.y, to.y)

    return MKMapPoint(x: x, y: y)
}

private func cubicBezier(_ t: Double, _ start: Double, _ c1: Double, _ c2: Double, _ end: Double) -> Double {
    let _t = 1 - t
    let _t² = _t * _t
    let _t³ = _t * _t * _t
    let t² = t * t
    let t³ = t * t * t

    return  _t³ * start +
        3.0 * _t² * t * c1 +
        3.0 * _t * t² * c2 +
        t³ * end
}
