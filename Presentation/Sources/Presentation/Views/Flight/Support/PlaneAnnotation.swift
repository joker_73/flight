import MapKit


final class PlaneAnnotation: NSObject, MKAnnotation {
    @objc dynamic var coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D()
    var nextCoordinate: CLLocationCoordinate2D?

    var mapPoint: MKMapPoint?
    var nextMapPoint: MKMapPoint?
}
