import MapKit


extension MKMapView {
    func register<T>(_ viewClass: AnyClass?, forAnnotationViewWithType type: T.Type) {
        register(viewClass, forAnnotationViewWithReuseIdentifier: String(describing: T.self))
    }

    func dequeueReusableAnnotationView(for annotation: MKAnnotation) -> MKAnnotationView {
        let identifier = String(describing: type(of: annotation))
        return dequeueReusableAnnotationView(withIdentifier: identifier, for: annotation)
    }
}
