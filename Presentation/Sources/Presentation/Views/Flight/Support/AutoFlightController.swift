import QuartzCore


final class AutoFlightController {
    var isStarted: Bool { displayLink != nil }

    private unowned let flightView: FlightUIView
    private let stateDicChangeHandler: (Bool) -> Void

    private var displayLink: CADisplayLink?
    private var duration: CFTimeInterval = 0
    private var startTimestamp: CFTimeInterval = 0


    init(flightView: FlightUIView, stateDicChangeHandler: @escaping (Bool) -> Void) {
        self.flightView = flightView
        self.stateDicChangeHandler = stateDicChangeHandler
    }

    deinit {
        stop()
    }
}


extension AutoFlightController {
    func start(duration: CFTimeInterval) {
        assert(Thread.current.isMainThread)

        guard isStarted == false else { return }

        self.duration = duration
        startTimestamp = CACurrentMediaTime()

        displayLink = CADisplayLink(target: self, selector: #selector(self.tick(displayLink:)))
        displayLink?.add(to: .current, forMode: .default)
        displayLink?.isPaused = false

        stateDicChangeHandler(true)
    }

    func stop() {
        assert(Thread.current.isMainThread)

        displayLink?.isPaused = true
        displayLink?.invalidate()
        displayLink = nil

        stateDicChangeHandler(false)
    }
}


private extension AutoFlightController {
    @objc func tick(displayLink: CADisplayLink) {
        assert(Thread.current.isMainThread)

        let progress = Float((displayLink.timestamp - startTimestamp) / duration)

        assert(progress >= 0)

        if progress > 1 {
            flightView.set(progress: 1)
            stop()
        } else {
            flightView.set(progress: progress)
        }
    }
}
