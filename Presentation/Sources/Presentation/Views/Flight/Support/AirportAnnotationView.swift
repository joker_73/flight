import UIKit
import MapKit


final class AirportAnnotationView: MKAnnotationView {
    private enum Constants {
        static let foregroundColor: UIColor? = UIColor(nameInModule: "Airport")?.withAlphaComponent(0.7)
        static let bacgrounddColor: UIColor? = UIColor(nameInModule: "AirportBackground")?.withAlphaComponent(0.45)
        
    }

    private let label = UILabel()


    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }


    override func draw(_ rect: CGRect) {
        Constants.foregroundColor?.setStroke()
        Constants.bacgrounddColor?.setFill()

        let rect = rect.insetBy(dx: 4, dy: 4)

        let shape = UIBezierPath(roundedRect: rect, cornerRadius: 50)
        shape.lineWidth = 4
        shape.fill()
        shape.stroke()
    }

    override func prepareForDisplay() {
        super.prepareForDisplay()
        label.text = annotation?.title ?? ""
    }
}


private extension AirportAnnotationView {
    func setupView() {
        frame = CGRect(x: 0, y: 0, width: 73, height: 42)
        backgroundColor = .clear

        label.font = UIFont.monospacedSystemFont(ofSize: 20, weight: .bold)
        label.textColor = Constants.foregroundColor
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)

        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: centerXAnchor),
            label.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
}
