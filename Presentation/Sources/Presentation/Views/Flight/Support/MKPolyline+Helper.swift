import MapKit


extension MKPolyline {
    subscript(index: Int) -> MKMapPoint? {
        guard index >= 0, index < pointCount else {
            return nil
        }

        let pointee = points()
        return pointee[index]
    }
}
