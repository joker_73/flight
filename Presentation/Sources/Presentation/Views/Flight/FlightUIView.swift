import UIKit
import MapKit


final class FlightUIView: UIView {
    private enum Constants {
        static let flightLineWidth: CGFloat = 4
        static let flightLineDashPattern: [NSNumber] = [0, 8]
    }


    var regionDidChangeCallback: (() -> Void)?
    var autoFlightDidChangeStateCallback: ((Bool) -> Void)?

    var isAutoFlightStarted: Bool { autoFlightController.isStarted }

    private(set) var sourceAnnotation: MKAnnotation?
    private(set) var destinationAnnotation: MKAnnotation?

    private let mapView: MKMapView = .init()
    private lazy var autoFlightController: AutoFlightController = {
        AutoFlightController(flightView: self) { [weak self] in
            self?.autoFlightDidChangeStateCallback?($0)
        }
    }()

    private var flightPolyline: MKPolyline?
    private var planeAnnotation: PlaneAnnotation?


    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    deinit {
        autoFlightController.stop()
    }


    override func removeFromSuperview() {
        super.removeFromSuperview()
        autoFlightController.stop()
    }
}


extension FlightUIView {
    func set(
        source: CLLocationCoordinate2D,
        sourceTitle: String,
        destination: CLLocationCoordinate2D,
        destinationTitle: String
    ) {
        autoFlightController.stop()
        mapView.removeAnnotations(mapView.annotations)
        mapView.removeOverlays(mapView.overlays)

        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.title = sourceTitle
        sourceAnnotation.coordinate = source
        mapView.addAnnotation(sourceAnnotation)
        self.sourceAnnotation = sourceAnnotation

        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = destinationTitle
        destinationAnnotation.coordinate = destination
        mapView.addAnnotation(destinationAnnotation)
        self.destinationAnnotation = destinationAnnotation

        let flightPolyline = MKPolyline.bezier(source: source, destination: destination)
        mapView.addOverlay(flightPolyline)
        self.flightPolyline = flightPolyline

        let planeAnnotation = PlaneAnnotation()
        flightPolyline[0].map {
            planeAnnotation.coordinate = $0.coordinate
        }
        flightPolyline[1].map {
            planeAnnotation.nextCoordinate = $0.coordinate
        }
        mapView.addAnnotation(planeAnnotation)
        self.planeAnnotation = planeAnnotation
    }

    func set(progress: Float) {
        assert(Thread.current.isMainThread)

        guard progress >= 0, progress <= 1 else {
            assertionFailure("Progress should be in the range [0..1] <\(progress)>")
            return
        }

        guard let pointCount = flightPolyline?.pointCount else {
            assertionFailure("Point count is undefined <\(flightPolyline?.pointCount ?? -1)>")
            return
        }

        let index = Int(round((Float(pointCount) - 1) * progress))

        assert(flightPolyline?[index]?.coordinate != nil)
        planeAnnotation?.coordinate = flightPolyline?[index]?.coordinate ?? kCLLocationCoordinate2DInvalid
        planeAnnotation?.mapPoint = flightPolyline?[index]

        planeAnnotation?.nextCoordinate = flightPolyline?[index + 1]?.coordinate
        planeAnnotation?.nextMapPoint = flightPolyline?[index + 1]

        planeAnnotation
            .flatMap { mapView.view(for: $0) }
            .flatMap { $0 as? PlaneAnnotationView}
            .map { $0.updateAngle() }
    }

    func showWholeFlight() {
        let anotations = mapView.annotations.filter { !($0 is PlaneAnnotation) }
        mapView.showAnnotations(anotations, animated: true)
    }

    func startAutoFlight(duration: CFTimeInterval = 10) {
        autoFlightController.start(duration: duration)
    }

    func stopAutoFlight() {
        autoFlightController.stop()
    }
}


extension FlightUIView: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let view = mapView.dequeueReusableAnnotationView(for: annotation)

        view.collisionMode = .none
        if annotation is PlaneAnnotation {
            view.zPriority = .max
        }

        return view
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let polyline = overlay as? MKPolyline {
            let renderer = MKPolylineRenderer(polyline: polyline)
            renderer.strokeColor = UIColor(nameInModule: "FlightLine")
            renderer.lineWidth = Constants.flightLineWidth
            renderer.lineDashPattern = Constants.flightLineDashPattern
            return renderer
        }

        assertionFailure("Unknown overlay")
        return MKOverlayRenderer()
    }

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        regionDidChangeCallback?()
    }
}


private extension FlightUIView {
    func setupView() {
        // TODO: Необходимо учитывать поворт карты при вращении самолётика
        mapView.isRotateEnabled = false
        mapView.layoutMargins = .init(top: 20, left: 20, bottom: 20, right: 20)
        mapView.register(AirportAnnotationView.self, forAnnotationViewWithType: MKPointAnnotation.self)
        mapView.register(PlaneAnnotationView.self, forAnnotationViewWithType: PlaneAnnotation.self)
        mapView.delegate = self
        mapView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(mapView)

        NSLayoutConstraint.activate([
            mapView.topAnchor.constraint(equalTo: topAnchor),
            mapView.leadingAnchor.constraint(equalTo: leadingAnchor),
            mapView.bottomAnchor.constraint(equalTo: bottomAnchor),
            mapView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
    }
}
