import UIKit
import SwiftUI
import CoreLocation
import MapKit.MKAnnotation


struct FlightView: UIViewRepresentable {
    typealias UIViewType = FlightUIView


    @Binding var source: PlaceModel
    @Binding var destination: PlaceModel

    @Binding var isFlightFullyVisible: Bool
    @Binding var isAutoFlightEnabled: Bool


    init(
        source: Binding<PlaceModel>,
        destination: Binding<PlaceModel>,
        isFlightFullyVisible: Binding<Bool> = .constant(false),
        isAutoFlightEnabled: Binding<Bool> = .constant(false)
    ) {
        self._source = source
        self._destination = destination
        self._isFlightFullyVisible = isFlightFullyVisible
        self._isAutoFlightEnabled = isAutoFlightEnabled
    }


    func makeUIView(context: Context) -> UIViewType {
        let uiView = FlightUIView()

        DispatchQueue.main.async {
            uiView.regionDidChangeCallback = {
                isFlightFullyVisible = false
            }
            uiView.autoFlightDidChangeStateCallback = {
                isAutoFlightEnabled = $0
            }

            isFlightFullyVisible = false
            //isAutoFlightEnabled = false
        }

        return uiView
    }

    func updateUIView(_ uiView: UIViewType, context: Context) {
        if !source.isEquals(uiView.sourceAnnotation) || !destination.isEquals(uiView.destinationAnnotation) {
            uiView.set(
                source: source.location.clLocationCoordinate,
                sourceTitle: source.iata,
                destination: destination.location.clLocationCoordinate,
                destinationTitle: destination.iata
            )
        }

        if isFlightFullyVisible {
            uiView.showWholeFlight()
        }

        if isAutoFlightEnabled && (uiView.isAutoFlightStarted == false) {
            uiView.startAutoFlight()
        }
    }
}


private extension PlaceModel {
    func isEquals(_ annotation: MKAnnotation?) -> Bool {
        return annotation.flatMap { $0.isEqual(annotation) } ?? false
    }

    func isEquals(_ annotation: MKAnnotation) -> Bool {
        let clLocation = location.clLocationCoordinate
        return clLocation.latitude == annotation.coordinate.latitude
            && clLocation.longitude == annotation.coordinate.longitude
            && iata == annotation.title
    }
}


