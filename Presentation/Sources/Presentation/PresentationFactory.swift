import Domain
import SwiftUI


public enum PresentationFactory {
    public static func flightSearchView(airportSearchUseCase: IAirportSearchUseCase) -> some View {
        return FlightSearchView()
            .environmentObject(AirportSearchUseCaseWrapper(airportSearchUseCase: airportSearchUseCase))
    }
}
