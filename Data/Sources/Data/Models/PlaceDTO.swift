import Foundation


struct PlaceDTO: Decodable {
    let countryIata: String
    let location: CoordinateDTO
    let airportName: String?
    let iata: String
    let coordinates: [Double]
    let cityIata: String?
    let name: String
}
