import Foundation


struct CoordinateDTO: Decodable, Equatable {
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lon"
    }


    let latitude: Double
    let longitude: Double
}
