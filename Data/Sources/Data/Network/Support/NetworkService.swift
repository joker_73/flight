import Foundation
import Combine



enum NetworkServiceError: Error {
    case wrongURLComponents(URLComponents?)
}


enum NetworkServiceHTTPMethod: String {
    case get = "GET"
    case post = "POST"
}


struct NetworkServiceResponse<Value> {
    let value: Value
    let response: URLResponse
}


protocol NetworkService {
    var base: String { get }
    var urlSession: URLSession { get }
    var jsonDecoder: JSONDecoder { get }
    var commonQueryItems: [String: String?] { get }
    var commonHTTPHeaders: [String: String] { get }
}


extension NetworkService {
    func run<Value>(
        method: NetworkServiceHTTPMethod = .get,
        path: String = "",
        queryItems: [String: String?] = [:],
        httpHeaders: [String: String] = [:]
    ) -> AnyPublisher<NetworkServiceResponse<Value>, Error> where Value: Decodable {
        var components = URLComponents(string: base)
        components?.queryItems = queryItems
            .merging(commonQueryItems) { current, _ in current }
            .map { key, value in URLQueryItem(name: key, value: value)}

        guard var url = components?.url else {
            assertionFailure("Wrong URL components: <\(String(describing: components))>")
            return Fail(error: NetworkServiceError.wrongURLComponents(components))
                .eraseToAnyPublisher()
        }

        url.appendPathComponent(path)

        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        httpHeaders.merging(commonHTTPHeaders) { current, _ in current }
            .forEach { key, value in request.addValue(value, forHTTPHeaderField: key) }

        return urlSession
            .dataTaskPublisher(for: request)
            .tryMap { [jsonDecoder] in
                let value = try jsonDecoder.decode(Value.self, from: $0.data)
                return NetworkServiceResponse(value: value, response: $0.response)
            }
            .subscribe(on: DispatchQueue.global())
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}
