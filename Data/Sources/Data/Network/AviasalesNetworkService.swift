import Foundation
import Combine


struct AviasalesNetworkService: NetworkService {
    let base: String = "https://places.aviasales.ru"
    let urlSession: URLSession = .shared
    private(set) var jsonDecoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        return jsonDecoder
    }()
    var commonQueryItems: [String : String?] {[
        "locale": NSLocale.current.identifier,
    ]}
    var commonHTTPHeaders: [String : String] {[:]}


    // Example: https://places.aviasales.ru/places?term=париж&locale=ru
    func places(term: String) -> AnyPublisher<[PlaceDTO], Error> {
        let queryItems = [
            "term": term,
        ]

        return run(path: "places", queryItems: queryItems)
            .map { $0.value }
            .eraseToAnyPublisher()
    }
}
