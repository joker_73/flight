import Combine
import Domain


final class PlaceRepository: IPlaceRepository {
    private let aviasales: AviasalesNetworkService = .init()


    func places(term: String) -> AnyPublisher<[Place], Error> {
        return aviasales.places(term: term)
            .map { placesDTO in
                placesDTO.map {
                    Place(placeDTO: $0)
                }
            }
            .eraseToAnyPublisher()
    }
}


extension Coordinate {
    init(coordinateDTO: CoordinateDTO) {
        self.init(
            latitude: coordinateDTO.latitude,
            longitude: coordinateDTO.longitude
        )
    }
}


extension Place {
    init(placeDTO: PlaceDTO) {
        self.init(
            countryIata: placeDTO.countryIata,
            location: Coordinate(coordinateDTO: placeDTO.location),
            airportName: placeDTO.airportName,
            iata: placeDTO.iata,
            coordinates: placeDTO.coordinates,
            cityIata: placeDTO.cityIata,
            name: placeDTO.name
        )
    }
}
